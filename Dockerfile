FROM openjdk:17-alpine
RUN addgroup -S my_group && adduser -S my_user -G my_group
USER my_user
COPY target/test-service*.jar /app/test-service.jar
ENTRYPOINT ["java", "-jar", "/app/test-service.jar"]
